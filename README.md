# Deployment of MobileNetV2 and Resnet50 on Jetson Nano

This project deploys a MobileNetV2 network and a Resnet50 network on Jetson Nano.

## On the Host PC

Model is obtained from keras in `train_mobilenetv2.py` and `train_resnet50.py`:

```python
model = tf.keras.applications.mobilenet_v2.MobileNetV2(
    include_top=True,
    weights='imagenet',
    input_tensor=None,
    input_shape=None,
    pooling=None,
    classes=1000
)
```

Then make the onnx file:

```python
# pip install tf2onnx
python -m tf2onnx.convert --saved-model resnet50_saved_model --output resnet50.onnx
python -m tf2onnx.convert --saved-model mobilenetv2_saved_model --output mobilenetv2.onnx
```

Send onnx file to Edge using `scp`.

## On the Edge

### Setup

Using 2 groups of 5V-GND pins to power the Jetson Nano.

For the multimeter DMM6500, use the rear ports.

Create a virtual envirment to install `tensorrt` and other dependencies:

```bash
pip3 install cython
pip3 install markupsafe
pip3 install platformdirs, dataclasses, typing-extensions, pytools, appdirs, MarkupSafe, mako, pycuda
```

### Inference

See the `Edge` folder.

Convert the onnx model to trt and inference for 1000 times:

```bash
/usr/src/tensorrt/bin/trtexec --onnx=mobilenetv2.onnx --saveEngine=mobilenetv2.trt
(zni) eavise@eavise-jetson:~/zijie_paper$ python3 run_trt.py 
```
