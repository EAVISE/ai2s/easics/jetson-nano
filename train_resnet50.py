'''
Date: 2024-05-07 09:58:38
Author: Zijie Ning zijie.ning@kuleuven.be
LastEditors: Zijie Ning zijie.ning@kuleuven.be
LastEditTime: 2024-07-04 11:25:02
FilePath: /jetson/train_resnet50.py
'''
import numpy as np
import tensorflow as tf
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.utils import to_categorical

# Instantiate the model
model = tf.keras.applications.ResNet50(
    include_top=True,
    weights='imagenet',
    input_tensor=None,
    input_shape=None,
    pooling=None,
    classes=1000
)

# Compile the model
model.compile(optimizer=Adam(),
              loss='categorical_crossentropy',
              metrics=['accuracy'])

# Fake data for demonstration: 10 example images with 1000 classes
fake_data = np.random.random((10, 224, 224, 3))
fake_labels = to_categorical(np.random.randint(1000, size=(10)), num_classes=1000)

# Train the model (fake training)
model.fit(fake_data, fake_labels, epochs=1)

# Save the model
model.save('resnet50.h5')

# Save to SavedModel format
saved_model_dir = "./resnet50_saved_model"
tf.saved_model.save(model, saved_model_dir)

model.summary()
print("Model training simulated and saved successfully.")
