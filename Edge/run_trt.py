import tensorrt as trt
import pycuda.driver as cuda
import pycuda.autoinit
import numpy as np

# Load trt engine
def load_engine(engine_path):
    TRT_LOGGER = trt.Logger(trt.Logger.WARNING)
    with open(engine_path, 'rb') as f, trt.Runtime(TRT_LOGGER) as runtime:
        return runtime.deserialize_cuda_engine(f.read())

# Inference
def do_inference(engine, image):
    context = engine.create_execution_context()
    input_shape = engine.get_binding_shape(0)
    output_shape = engine.get_binding_shape(1)
    
    # Allocate memory
    d_input = cuda.mem_alloc(trt.volume(input_shape) * trt.float32.itemsize)
    d_output = cuda.mem_alloc(trt.volume(output_shape) * trt.float32.itemsize)
    
    bindings = [int(d_input), int(d_output)]
    stream = cuda.Stream()
    
    # Copy image data to gpu
    cuda.memcpy_htod_async(d_input, image, stream)
    
    # Run inference
    context.execute_async(bindings=bindings, stream_handle=stream.handle)
    
    # Get results
    output = np.empty(output_shape, dtype=np.float32)
    cuda.memcpy_dtoh_async(output, d_output, stream)
    stream.synchronize()
    
    return output

# Fake input
def generate_random_input(input_shape):
    return np.random.rand(*input_shape).astype(np.float32)

# Main func
def main():
    # engine_path = 'resnet50.trt'
    engine_path = 'mobilenetv2.trt'
    # image_path = 'test_image.jpg'
    
    engine = load_engine(engine_path)
    input_shape = engine.get_binding_shape(0)
    
    # image = preprocess_image(image_path, input_shape)
    image = generate_random_input(input_shape)

    
    # Copy image to GPU
    image = np.ascontiguousarray(image)
    for i in range (1000):
        output = do_inference(engine, image)
        # print(i)
    
    # print("Inference Output:", output)

if __name__ == '__main__':
    main()
